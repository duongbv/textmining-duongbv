# -*- coding: utf-8 -*-
"""
Created on Fri Nov 18 16:10:07 2016

@author: duongbv1993
"""

import nltk.data
tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
file = open('D:/MASTER/textmining/slot3/nlp.txt', 'r+')

"""
ex1
"""
newDoc =""
tokens = tokenizer.tokenize(file.read())
for line in tokens:
    newDoc += line + "\n"
  
"""
ex2
"""

from nltk.tokenize import TreebankWordTokenizer
tokenizer = TreebankWordTokenizer()

newDoc2 = ""
tokens2 = tokenizer.tokenize(newDoc)
for line in tokens2:
    newDoc2 += line  + "\n"
print(newDoc2)    