# -*- coding: utf-8 -*-
"""
Created on Sat Nov 12 10:11:30 2016

@author: duongbv1993
"""

import nltk
tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
file = open('D:/MASTER/textmining/slot3/nlp.txt', 'r+')

"""
ex1
"""
newDoc =""
tokens = tokenizer.tokenize(file.read())
for line in tokens:
    newDoc += line + "\n"   
"""
ex2
"""
from nltk.tokenize import TreebankWordTokenizer
tokenizer = TreebankWordTokenizer()

newDoc2 = ""
tokens2 = tokenizer.tokenize(newDoc)
    
"""
ex4
"""
newDoc3 = ""
wnl = nltk.WordNetLemmatizer()
for word in tokens2:
    newDoc3 += word + "\t" + wnl.lemmatize(word) + "\n"
print(newDoc3)

