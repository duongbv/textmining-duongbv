# -*- coding: utf-8 -*-
"""
Created on Fri Nov 18 16:37:31 2016

@author: duongbv1993
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Nov 12 10:11:30 2016

@author: duongbv1993
"""

import nltk
tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
file = open('D:/MASTER/textmining/slot3/nlp.txt', 'r+')

"""
ex1
"""
newDoc =""
tokens = tokenizer.tokenize(file.read())
for line in tokens:
    newDoc += line + "\n"   
"""
ex2
"""
from nltk.tokenize import TreebankWordTokenizer
tokenizer = TreebankWordTokenizer()

newDoc2 = ""
tokens2 = tokenizer.tokenize(newDoc)
    
"""
ex5
"""
from nltk.corpus import stopwords
english_stops = set(stopwords.words('english'))
newDoc3 = ""
for word in tokens2:
    if word not in english_stops:
        newDoc3 += word + "\n"
print(newDoc3)

