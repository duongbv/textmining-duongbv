""" Example code of Viterbi algorithm on Wikipedia
    Reference: https://en.wikipedia.org/wiki/Viterbi_algorithm#Example
"""

def viterbi(states):
    V = [{}]
    ictrain = open('ictrain', 'r+')
    ictest = open('ictest', 'r+')
    obs= []
    result = []
    for line in ictest:
        if line[0] != '#':
            obs.append(line[0])
            result.append(line[2])
    countH = 0
    countC = 0
    previous = ""
    hToH = 0
    hToC = 0
    cToH = 0
    cToC = 0
    for line in ictrain:
        if line[0] != '#':
            if line[2] == 'H':
                countH += 1
                if previous != "":
                    if line[2] == previous:
                        hToH +=1
                    if line[2] != previous:
                        hToC +=1
            if line[2] == 'C':
                countC += 1
                if previous != "":
                    if line[2] == previous:
                        cToC +=1
                    if line[2] != previous:
                        cToH +=1
            previous = line[2]
        if line[0] == '#':
            previous = ""
    start_p = {'H': countH / (countH + countC), 'C': countC / (countH + countC)}
    trans_p = {
	   'H' : {'H': (hToH/(hToC+hToH)), 'C': (hToC/(hToC+hToH))},
	   'C' : {'H': (cToH/(cToC+cToH)), 'C': (cToC/(cToC+cToH))}
    }
    hWith1 = 0
    hWith2 = 0
    hWith3 = 0
    cWith1 = 0
    cWith2 = 0
    cWith3 = 0
    ictrain1 = open('ictrain', 'r+')
    for line in ictrain1:
        if line[0] != '#':
            if line[0] == '1':
                if line[2] == 'H':
                    hWith1 += 1
                if line[2] == 'C':
                    cWith1 += 1
            if line[0] == '2':
                if line[2] == 'H':
                    hWith2 += 1
                if line[2] == 'C':
                    cWith2 += 1
            if line[0] == '3':
                if line[2] == 'H':
                    hWith3 += 1
                if line[2] == 'C':
                    cWith3 += 1
    
    emit_p = {
	   'H' : {'1': (hWith1/(hWith1+hWith2+hWith3)), '2': (hWith2/(hWith1+hWith2+hWith3)), '3': (hWith3/(hWith1+hWith2+hWith3))},
	   'C' : {'1': (cWith1/(cWith1+cWith2+cWith3)), '2': (cWith2/(cWith1+cWith2+cWith3)), '3': (cWith3/(cWith1+cWith2+cWith3))}
    }
    
    for st in states:
        V[0][st] = {"prob": start_p[st] * emit_p[st][obs[0]], "prev": None}
    
    # Run Viterbi when t > 0
    for t in range(1, len(obs)):
        V.append({})
        for st in states:
            
            max_tr_prob = max(V[t-1][prev_st]["prob"]*trans_p[prev_st][st] for prev_st in states)
            for prev_st in states:
                if V[t-1][prev_st]["prob"] * trans_p[prev_st][st] == max_tr_prob:
                    max_prob = max_tr_prob * emit_p[st][obs[t]]
                    V[t][st] = {"prob": max_prob, "prev": prev_st}
                    break

    opt = []

    # The highest probability
    max_prob = max(value["prob"] for value in V[-1].values())
    previous = None

    # Get most probable state and its backtrack
    for st, data in V[-1].items():
        if data["prob"] == max_prob:
            opt.append(st)
            previous = st
            break

    # Follow the backtrack till the first observation
    for t in range(len(V) - 2, -1, -1):
        opt.insert(0, V[t + 1][previous]["prev"])
        previous = V[t + 1][previous]["prev"]

    print('predict with ictest ' + ' '.join(opt))
    match = 0
    for index in range(len(opt)):
        if result[index] == opt[index]:
            match+=1
    acc = match/len(opt) * 100
    print('Tagging accuracy: '+ str(acc) + '%') 


def main():
	states = ('H', 'C')
	viterbi(states)


if __name__ == '__main__':
    main()