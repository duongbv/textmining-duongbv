# -*- coding: utf-8 -*-
"""
@author: DuongBV
@adding comments: HungLM
"""
<<<<<<< HEAD

TAG_SET = ['$$START', '$START', '$END', 'at', 'np-tl', 'nn-tl', 'jj-tl', 'vbd', 'nr', 'nn', 'in', 'np$', 'jj', '``', "''", 'cs', 'dti', 'nns', '.', 'rbr', ',', 'wdt', 'hvd','bedz', 'vbn', 'np', 'ben', 'to', 'vb', 'wrb', 'cd', 'md', 'be', 'jjr', 'vbg-tl', 'bez', 'nn$-tl', 'hvz', 'abn', 'pn', 'ppss', 'pp$', 'do', 'wps', '*', 'ex','UNK']

V = set() 
=======
#tag set get from https://en.wikipedia.org/wiki/Brown_Corpus
TAG_SET = ['$$START', '$START', '$END', ".", "(", ")", "*", "--", ",", ":", "abl", "abn", "abx", "ap", "at", "be", "bed", "bedz", "beg", "bem", "ben", "ber", "bez", "cc", "cd", "cs", "do", "dod", "doz", "dt", "dti", "dts", "dtx", "ex", "fw", "hv", "hvd", "hvg", "hvn", "in", "jj", "jjr", "jjs", "jjt", "md", "nc", "nn", "nn$", "nns", "nns$", "np", "np$", "nps", "nps$", "nr", "od", "pn", "pn$", "pp$", "pp$$", "ppl", "ppls", "ppo", "pps", "ppss", "prp", "prp$", "ql", "qlp", "rb", "rbr", "rbt", "rn", "rp", "to", "uh", "vb", "vbd", "vbg", "vbn", "vbp", "vbz", "wdt", "wp$", "wpo", "wps", "wql", "wrb", 'UNK']
V = set()
>>>>>>> a63d2bd83b961292031b74da336e998f82b4c03c
transition = {}
emission = {}

transition_bigram = {}
context = {}
emit = {}

'''
	for calculating smoothing factors
	in some books, l1 is Lambda, l2 is 1 - Lambda, kk == 1
	kk equals 1 in short formular, we use the more general formular
	l2 not necessary equals 1 - l1, but (l1 + l2) must not > 1
'''
kk = 0.1	
l1 = 0.1 	
l2 = 0		

# working with files and directories, using Unix/Linux pattern
import glob 

def viterbi(sentence):
	dp = []			# for storing max probability
	tag_seq = []	# for storing tag with highest probability

	# FORWARD
	
	# Initiate the main arrays
	for i in range(len(sentence)):
		dp.append([])
		tag_seq.append([])
		for j in range(len(TAG_SET)):
			dp[i].append([])
			tag_seq[i].append([])
			for each in TAG_SET:
				dp[i][j].append(0)
				tag_seq[i][j].append(0)

	# Calculate transition from '$$START' and emission of the first word for every tag
	for i in range(len(TAG_SET)):
		tag_gram = ('$$START', '$START', TAG_SET[i])
		trans = ip_laplace(tag_gram, 0)
		word_gram = ('$START', TAG_SET[i], sentence[0][0])
		emit = ip_laplace(word_gram, 1)
		dp[0][1][i] = trans*emit

	# Calculate for the middle tags
	for p in range(1, len(sentence)):
		for k in range(len(TAG_SET)):
			for j in range(len(TAG_SET)):
				maximum = -1
				word_gram = (TAG_SET[j], TAG_SET[k], sentence[p][0])
				emit = ip_laplace(word_gram, 1)
				for i in range(len(TAG_SET)):
					tag_gram = (TAG_SET[i], TAG_SET[j], TAG_SET[k])
					trans = ip_laplace(tag_gram, 0)
					temp = dp[p-1][i][j] * trans
					if temp >= maximum:
						maximum = temp
						max_pos = i
				dp[p][j][k] = maximum * emit
				tag_seq[p][j][k] = max_pos
	
	# Add the end tag
	final_tags = []		
	maximum = -1		
	for i in range(len(TAG_SET)):
		for j in range(len(TAG_SET)):
			temp = dp[-1][i][j]
			if temp > maximum:
				maximum = temp
				max_i = i
				max_j = j
	final_tags.append(TAG_SET[max_j])
	final_tags.append(TAG_SET[max_i])
	# FORWARD END
	
	# BACKWARD
	p = len(sentence) - 1
	while p >= 0:
		next = tag_seq[p][max_i][max_j]
		max_j = max_i
		max_i = next
		final_tags.append(TAG_SET[next])
		p -= 1
	final_tags.reverse()
	# BACKWARD END
	
	return final_tags

def preprocess(data):
	global V, transition_bigram, context, emit
	
	# Initiate the vocabulary set
	sentences = parse(data)		
	for sentence in sentences:
		for word in sentence:
                  V.add(word[0])
                  
	# Initiate the transition biagram dictionary
	for sentence in sentences:
		# first biagram tuple
		tag_gram = ('$$START', '$START')
		# if there is already a key same as first biagram, the counter/value + 1
		transition_bigram[tag_gram] = transition_bigram.get(tag_gram, 0) + 1
		# second biagram tuple: ('$START', first word) 
		tag_gram = ('$START', sentence[0][0])
		# if there is already a key same as second biagram, the counter/value + 1
		transition_bigram[tag_gram] = transition_bigram.get(tag_gram, 0) + 1	
		# traverse the rest of this sentence, if there is already a key same as current biagram, the counter/value + 1		
		i = 1		
		while i < len(sentence):
			tag_gram = (sentence[i-1][1], sentence[i][1])			
			transition_bigram[tag_gram] = transition_bigram.get(tag_gram, 0) + 1
			i += 1
	
	# Initiate the frequency for tags
	for sentence in sentences:
		context['$$START'] = context.get('$$START', 0) + 1
		context['$START'] = context.get('$START', 0) + 1
		for word in sentence:
			context[word[1]] = context.get(word[1], 0) + 1
	
	# Initiate the frequency of words that label with such tag/part of speech
	for sentence in sentences:
		for word in sentence:
			word_gram = (word[1], word[0])
			emit[word_gram] = emit.get(word_gram, 0) + 1
	
	return sentences

# get the transition and assign to the global variable of same name (belongs to the data structure: dictionary)
# after this function, we will have a dictionary of {transition triagram, appearing times of such triagram}
def find_transition(sentences):
	global transition
	for sentence in sentences:
		
		# the first triagram tuple, consist of ('$$START', '$START', first tag)
		trigram = ('$$START', '$START', sentence[0][1])
		# if there is already a key same as first triagram, the counter/value + 1
		transition[trigram] = transition.get(trigram, 0) + 1

		# the second triagram tuple, consist of ('$START', first tag, second tag)
		trigram = ('$START', sentence[0][1], sentence[1][1])
		# if there is already a key same as second triagram, the counter/value + 1
		transition[trigram] = transition.get(trigram, 0) + 1
		
		# traverse the rest of this sentence, if there is already a key same as current triagram, the counter/value + 1
		i = 2
		while i < len(sentence):
			trigram = (sentence[i-2][1], sentence[i-1][1], sentence[i][1])
			transition[trigram] = transition.get(trigram, 0) + 1
			i += 1

# Calculate the transition/emission probability with Laplace smoothing
def ip_laplace(ngram, type):

	wn_0 = ngram[0]
	wn_1 = ngram[1]
	wn_2 = ngram[2]

	# transition smoothing
	if type == 0:
		x1 = l1*1.0*(transition.get( (wn_0, wn_1, wn_2) , 0) + kk)/(transition_bigram.get( (wn_0, wn_1) , 0) + kk*len(context.keys()) )
		x2 = l2*1.0*(transition_bigram.get( (wn_1, wn_2) , 0) + kk)/(context.get(wn_1, 0) + kk*len(context.keys()) )
	# emission smoothing
	else:
		x1 = l1*1.0*(emission.get( (wn_0, wn_1, wn_2) , 0) + kk)/(transition_bigram.get( (wn_0, wn_1) , 0) + kk*len(V) )
		x2 = l2*1.0*(emit.get( (wn_1, wn_2) , 0) + kk)/(context.get(wn_1, 0) + kk*len(V))
	return x1 + x2
 
# function for parsing data, in this program := a very long string consist of all lines of text in all files of train/test directory
def parse(data):
	# sentences will be an array of lines of text
	sentences = data.split('\n')
	# there are 2 blank lines between real text lines, eliminate them
	# with real text, trim it, then append a space + '$$/$END'
	sentences = [ ' '.join([each.strip(), '$$/$END']) for each in sentences if each.strip() != '' ]
	# sentences is now an array of: [ [word, tag], [word, tag],..] with ['$$', '$END'] as sentence end marker
	sentences = [ [every.split('/') for every in each.split(' ')] for each in sentences]
	return sentences
	
# get the emission and assign to the global variable of same name (belongs to the data structure: dictionary)
# after this function, we will have a dictionary of {emission triagram, appearing times of such triagram}
def find_emision(sentences):
	global emission
	for sentence in sentences:
		# the first triagram tuple, consist of ('$START', first tag, first word)
		trigram = ('$START', sentence[0][1], sentence[0][0])
		# if there is already a key same as first triagram, the counter/value + 1
		emission[trigram] = emission.get(trigram, 0) + 1
		
		# traverse the rest of this sentence, if there is already a key same as current triagram, the counter/value + 1
		i = 1
		while i < len(sentence):
			trigram = (sentence[i-1][1], sentence[i][1], sentence[i][0])
			emission[trigram] = emission.get(trigram, 0) + 1
			i += 1
	
# MAIN PROGRAM
train_fileDir = glob.glob('train/*')
train_data = ""
for fileName in train_fileDir:
    file = open(fileName, 'r+')
    train_data += file.read()
sentences = preprocess(train_data)

print(TAG_SET)
find_transition(sentences)
find_emision(sentences)

mx = (-1, -1, -1)	# for storing the best accuracy that can achieved
ans = (-1, -1, -1)	# for storing tuple of best (kk, l1, l2)

test_fileDir = glob.glob('test/*')
test_data = ""
for fileName in test_fileDir:
    file = open(fileName, 'r+')
    test_data += file.read()
test_sentences = parse(test_data)

'''
	Main process
	Loop with 
		k in range 0.1 to 1, step == 0.1
		l1 in range 0.1 to 1, step == 0.1
		l2 in range 0.1 to 1, step == 0.1
		(l1 + l2) <= 1
	To get the best smoothing factor tuple and its accuracy
'''
while kk<=1:
    
    l1 = 0.1
    while l1<=1:
        
        l2 = 0
        while l2<=1:
            if (l2 + l1) > 1:
                break
            sent_accuracy = 0
            correct_tokens = 0
            total_tokens = 0
            avg = 0
            for sent in test_sentences:
                token_accuracy = 0			

				# result of algorithm, exclude '$$START' and '$START'
                result = viterbi(sent)[2:]
				# the actual array of tags in test files
                answer = [word[1] for word in sent]
				
				# check if this sentence is 100% correct
                if result == answer:
                    sent_accuracy += 1
                else:
                    print("OURS", result)
                    print("REAL", answer)				
					
				# for checking, with current (kk, l1, l2)
				# the accuracy of tags in a sentence
				# the current average accuracy of each sentence
                total_tokens += len(result)
                for i in range(len(result)):
                    if result[i] == answer[i]:
                        correct_tokens += 1
                        token_accuracy += 1
                        avg += 1.0*token_accuracy/len(sent)
					
				# There are 3 kinds of accuracy, in order of priorities:
				# 1. Average accuracy of each sentence
				# 2. The rate of 100% correct sentence
				# 3. The accuracy of tags in all tags
					
				# Compare in such order to get: 
				# - The best accuracy that can achieved in mx tuple
				# - The best tuple of (kk, l1, l2)
                R1 = 1.0*avg/len(test_sentences)
                R2 = 1.0*sent_accuracy/len(test_sentences)
                R3 = 1.0*correct_tokens/total_tokens
                result = (R1, R2, R3)
                if result > mx:
                    mx = result
                    ans = (kk, l1, l2)
				
				# end of checking accuracy for a (kk, l1, l2) tuple, in crease l2 to check the next suitable
                l2 += 0.1
        l1 += 0.1
    kk += 0.1

# main process ended, print the result
print(ans)
print(mx)
# MAIN PROGRAM ENDED