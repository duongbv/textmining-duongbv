# -*- coding: utf-8 -*-
"""
Created on Fri Dec  2 23:10:35 2016

@author: duongbv1993
"""

TAG_SET = ['$$START', '$START', '$END']
V = set()
transition = {}
emission = {}
transition_bigram = {}
context = {}
emit = {}
def parse(data):
	sentences = data.split('\n')
	sentences = [ ' '.join([each.strip(), '$$/$END']) for each in sentences if each.strip() != '' ]
	sentences = [ [every.split('/') for every in each.split(' ')] for each in sentences]
	return sentences
def preprocess(data):
	global V, transition_bigram, context, emit
	sentences = parse(data)		
	for sentence in sentences:
		for word in sentence:
                  V.add(word[0])
                  addTag = 'True'
                  for tag in TAG_SET:
                      if tag == word[1]:
                          addTag = 'False'
                  if addTag == 'True':
                      lastIndex = len(TAG_SET) -1
                      TAG_SET.insert(lastIndex,word[1])
	
	return sentences
import glob
train_fileDir = glob.glob('train/*')
train_data = ""
for fileName in train_fileDir:
    file = open(fileName, 'r+')
    train_data += file.read()
sentences = preprocess(train_data)
print(TAG_SET)